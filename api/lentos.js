/*
    Lista apenas os pokemons com menos de 40 pontos de velocidade (speed)
    A lista deve estar ordenada do mais lento para o mais rápido
*/

var data = require('../data')

module.exports = function (req, res) {
    var pokemon = [];
    var result = data.pokemon

    result.forEach(element => {
        if (element.speed < 40) {
            pokemon.push(element);
        }
    });

    pokemon.sort(function(a,b) {
        return a.speed < b.speed ? -1 : a.speed > b.speed ? 1 : 0;
    });
    

    res.json(pokemon);
}